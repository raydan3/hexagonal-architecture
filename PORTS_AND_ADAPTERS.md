# HEXAGONAL ARCHITECTURE

**Hexagonal Architecture**, also known as **Ports and Adapters Architecture**, is a design pattern that promotes the separation of concerns by structuring the software into distinct layers. This structure makes the system more maintainable, scalable, and easier to test. The main layers of Hexagonal Architecture are as follows:

![plot](images/hexagonal_architecture.png)

## Layers

### Domain

- **Entities**: These are the core business objects that encapsulate the essential data and behavior of the domain.

- **Value Objects**: Immutable objects that describe certain aspects of the domain with no conceptual identity.

- **Domain Services**: Logic that doesn't naturally belong to an entity or value object, typically operations that involve multiple entities.

- **Aggregates and Aggregate Roots**: Group of related entities treated as a single unit to ensure consistency.

- **Repositories (Domain Interfaces)**: Abstractions that define the methods for persisting and retrieving aggregates.

### Application

- **Application Services (Use Cases)**: Define the application's behavior and coordinate the flow between the domain and external layers. They encapsulate the application's business logic without being aware of how the data is stored or retrieved.

- **Ports**: Interfaces that define the input (inbound ports) and output (outbound ports) interactions. They ensure that the application logic is decoupled from external technologies and services.

### Infrastructure

- **Adapters**: Concrete implementations of the ports, facilitating communication between the application and external systems.

- **Inbound Adapters**: Implement the inbound ports and handle external inputs like HTTP requests, messages from a queue, etc. These translate external requests into commands that the application layer can handle.

- **Outbound Adapters**: Implement the outbound ports and manage interactions with external systems such as databases, messaging systems, and third-party services. They convert application layer requests into the appropriate format for external systems.

- **Technical Components**: Configuration and integration specifics for databases, message brokers, external APIs, and other infrastructure concerns.

#### Dependency rule

The dependency rule mandates that dependencies should always point inward toward the domain layer (**Infrastructure -> Application -> Domain**).

This means that the **Domain** layer is independent of the **Application** and **Infrastructure** layers, ensuring that the core domain model and business logic remain decoupled from external technologies and frameworks.

#### How they interact

1. Domain Layer: Defines a `Book` entity and a `BookRepository` interface.

2. Application Layer: Implements a use case like `BookCreator` that uses the `BookRepository` to persist new books.

3. Infrastructure Layer: Provides an implementation of the `BookRepository` interface, perhaps using a relational database, as well as a REST controller (inbound adapter) that receives HTTP POST requests to register new customers and forwards them to the `BookCreator` use case.

## Domain Services vs. Application Services

### Domain Services

- **Purpose**: Encapsulate domain logic that doesn’t naturally fit within entities or value objects.
- **Role**: Participate in business decision-making processes, similar to how entities and value objects do.

### Application Services:

- **Purpose**: Orchestrate operations and interactions with the external world, such as databases and third-party services.
- **Role**: Serve as entry points to the application and coordinate domain operations without containing business logic themselves.

#### When to Use Domain Services

Domain services are used to isolate domain logic that can’t be attributed to entities or value objects without breaking their isolation.
They are particularly useful when the logic requires information from external sources or involves complex operations that don’t belong to a single entity.

#### Avoiding Overuse of Domain Services

Not all logic that spans multiple lines of code needs to be extracted into a domain service. The key consideration is whether the code is making business decisions.  
For simple CRUD operations, application services alone may suffice, as there is often no complex business logic to encapsulate.

#### Injecting Domain Services

- Impure (non-isolated) domain services, which interact with external systems (e.g., payment gateways), should not be injected into entities to maintain the isolation of the domain model.

- Pure (isolated) domain services, which are closed under entities and value objects and do not depend on external systems, can be safely referenced within entities and value objects.

## Value Objects

Value Objects (VO) are classes identified by the value they represent. They differ from entities in that if their value changes, they no longer represent the same concept.

For example, an entity like a Video has an identifier, and changing its title doesn't change its identity. However, a VideoUrl as a VO changes its identity if its value changes.

```php
// https://github.com/CodelyTV/cqrs-ddd-php-example/blob/master/src/Context/Video/Module/Video/Domain/VideoUrl.php

final class VideoUrl extends StringValueObject
{
    public function __construct(string $value)
    {
        $this->guardValidUrl($value); // Validation at instantiation. We do not allow a VideoUrl with a null value, for example.

        parent::__construct($value);
    }

    private function guardValidUrl(string $url)
    {
        if (false === filter_var($url, FILTER_VALIDATE_URL)) {
            throw new \InvalidArgumentException(
                sprintf('The url <%s> is not well formatted', $url)
            );
        }
    }
}
```

### Benefits of Modeling with VO

- **Domain Semantics**: Using domain-specific types like VideoUrl, UserRange, Rating, etc., makes the code more readable and expressive of domain concepts.

- **Cohesion**: By encapsulating related logic within the VO class, the logic is closer to the data it operates on, leading to:
    - **Avoiding Redundant Checks**: Once instantiated, a VO like VideoUrl is validated, eliminating the need for repeated null checks.

    - **Logic Magnet**: Initially, a VO might seem unnecessary, but as the application evolves, small logic fragments can be consolidated within VOs, preventing service or model bloat.
