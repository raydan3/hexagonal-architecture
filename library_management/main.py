import asyncio

from src.contexts.book.application.book_finder import BookFinder
from src.contexts.book.infrastructure.in_memory_book_repository import InMemoryBookRepository
from src.contexts.book.application.book_creator import BookCreator
from src.contexts.book.domain.book_id import BookId
from src.contexts.book.domain.book_not_found import BookNotFoundException


async def main():
    try:
        book_repository = InMemoryBookRepository()
        book_creator = BookCreator(book_repository)
        book_finder = BookFinder(book_repository)

        book_id = BookId.random()
        await book_creator.save(book_id, "ISBN", "Title", "Author", "Category")
        book = await book_finder.find(book_id)

        if book:
            print(f"Book found: {book.id}")

    except BookNotFoundException:
        print("Book not found")


if __name__ == "__main__":
    asyncio.run(main())
