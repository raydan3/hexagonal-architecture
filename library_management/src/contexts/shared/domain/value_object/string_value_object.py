from dataclasses import dataclass


@dataclass(frozen=True)
class StringValueObject:
    _value: str

    @property
    def value(self) -> str:
        return self._value
