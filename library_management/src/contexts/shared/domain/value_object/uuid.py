import uuid
from dataclasses import dataclass


@dataclass(frozen=True)
class Uuid:
    _value: str

    def __str__(self) -> str:
        return self._value

    def __post_init__(self) -> None:
        uuid.UUID(self._value)

    @property
    def value(self) -> str:
        return self._value

    @property
    def hex(self) -> str:
        return uuid.UUID(self._value).hex

    @staticmethod
    def random() -> str:
        return str(uuid.uuid4())
