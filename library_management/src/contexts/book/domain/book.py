from dataclasses import dataclass
from typing import Dict

from src.contexts.book.domain.book_id import BookId
from src.contexts.book.domain.book_isbn import BookIsbn
from src.contexts.book.domain.book_title import BookTitle
from src.contexts.book.domain.book_author import BookAuthor
from src.contexts.book.domain.book_category import BookCategory


@dataclass
class Book:
    id: BookId
    isbn: BookIsbn
    title: BookTitle
    author: BookAuthor
    category: BookCategory

    def update_title(self, title: str) -> None:
        self.title = BookTitle(title)

    def to_primitives(self) -> Dict:
        return {
            'id': self.id.value,
            'isbn': self.isbn.value,
            'title': self.title.value,
            'author': self.title.value,
            'category': self.category.value
        }
