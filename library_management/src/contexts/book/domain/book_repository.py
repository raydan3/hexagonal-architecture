from abc import ABC, abstractmethod
from typing import Optional

from src.contexts.book.domain.book import Book
from src.contexts.book.domain.book_id import BookId


class BookRepository(ABC):

    @abstractmethod
    async def save(self, book: Book) -> None:
        pass

    @abstractmethod
    async def find(self, id: BookId) -> Optional[Book]:
        pass
